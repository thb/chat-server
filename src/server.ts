import { prop, Typegoose, ModelType, InstanceType } from 'typegoose';
import { createServer, Server } from 'https';
import express = require('express');
import socketIo = require('socket.io');
import mongoose = require('mongoose');
const fs = require('fs');
const NodeRSA = require('node-rsa');

import { User } from './models/user';
import { Room } from './models/room';
import { Message } from './models/message';
import { Config } from './config';

export class ChatServer {
  public static readonly PORT : number = 8080;
  private app?: express.Application;
  private server?: Server;
  private io?: SocketIO.Server; private config?: Config;
  private users: User[];
  private rooms: Room[];

  constructor() {
    this.config = new Config();
    this.users = [];
    this.rooms = [];
    this.init();
    this.listen();
  }

  private init(): void {
    let options = {
      key: fs.readFileSync(this.config!.sslKeyFile),
      cert: fs.readFileSync(this.config!.sslCertFile)
    }

    this.app = express();
    this.server = createServer(options, this.app);
    this.io = socketIo(this.server);
  }

  private listen(): void {
    this.server!.listen(this.config!.port, () => {
      console.log('Running server on port %s', this.config!.port);
    });

    this.io!.on('connect', (socket: any) => {
      console.log('Connected client on port %s.', this.config!.port);
      socket.on('register', (user: User) => {
        user = this.register(user, socket);
        const room = this.createRoom(user.name!);
        this.joinRoom(socket, room, user);
        this.io!.to(socket.id).emit('register');
      });
      socket.on('authenticate', (data: {name: string, token: string}) => {
        this.authenticate(data, socket)
        let currentUser = null;
        for (let user of this.users) {
          if (user.name == data.name) {
            currentUser = user;
            break;
          }
        }
        for (let room of this.rooms) {
          if (room.name == data.name) {
            this.joinRoom(socket, room, currentUser!);
            break;
          }
        }
      });
      socket.on('message', (m: Message) => {
        console.log('[server](message): %s', JSON.stringify(m));
        this.io!.sockets.in(m.roomName!).emit('message', m);
      });

      socket.on('disconnect', () => {
        console.log('Client disconnected');
      });
    });
  }

  private register(user: User, socket: any): User {
    let newUser: User = new User();
    newUser.name = user.name;
    newUser.publicKey = user.publicKey;
    for(user of this.users) {
      if(user.name == newUser.name) {
        this.io!.to(socket.id).emit('rejected');
      }
    }
    this.users.push(newUser);
    //mongoose add
    //add id from mongoose
    return newUser;
  }

  private authenticate(data: {name: string, token: string}, socket: any) {
    for(let user of this.users) {
      if (user.name == data.name) {
        let key = new NodeRSA();
        key.importKey(user.publicKey);
        const content = key.decryptPublic(data.token);
        if (content == 'authenticate') {
          this.io!.to(socket.id).emit('authenticate');
          return;
        } else {
          this.io!.to(socket.id).emit('rejected');
          return;
        }
      }
    }
    this.io!.to(socket.id).emit('unknown');
  }

  private createRoom(name: string): Room {
    let room = new Room();
    room.name = name;
    this.rooms.push(room);
    //TODO: mongoose add
    //TODO: add id from mongoose
    return room;
  }

  private joinRoom(socket: any, room: Room, user: User) {
    this.io!.sockets.in(room.name!).emit('join', {user: user, room: room});
    socket.join(room.name);
    console.log(user.name, 'joins room', room.name);
  }

  public getApp(): express.Application {
    return this.app!;
  }
}
// mongoose.connect('mongodb://localhost:27017/test', { useNewUrlParser: true });

const server = new ChatServer();

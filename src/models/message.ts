import { prop, Typegoose, ModelType, InstanceType, Ref } from 'typegoose';
import { Room } from './room';

export class Message extends Typegoose {
  @prop({ required: true })
  content?: string;
  @prop({ required: true })
  publicKey?: string;
  @prop({ required: true })
  roomName?: string;
}

export const MessageModel = new Message().getModelForClass(Message, { schemaOptions: { timestamps: true } });

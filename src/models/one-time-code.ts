import { prop, Typegoose, ModelType, InstanceType, Ref } from 'typegoose';
import { Room } from './room';

export class OneTimeCode extends Typegoose {
  @prop({ required: true })
  code?: string;
  @prop({ required: true, ref: Room })
  chat?: Ref<Room>;
}

export const OneTimeCodeModel = new OneTimeCode().getModelForClass(OneTimeCode, { schemaOptions: { timestamps: true } });

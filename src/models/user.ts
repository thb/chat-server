import { prop, Typegoose, ModelType, InstanceType, Ref } from 'typegoose';
import { Room } from './room';

export class User extends Typegoose {
  @prop({ required: true, unique: true })
  name?: string;

  @prop({ required: true })
  publicKey?: string;

  @prop({ ref: Room })
  rooms?: Ref<Room>[];
}

export const UserModel = new User().getModelForClass(User, { schemaOptions: { timestamps: true } });

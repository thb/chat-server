import { prop, Typegoose, ModelType, InstanceType, Ref } from 'typegoose';
import { User } from './user';
import { Message } from './message';

export class Room extends Typegoose {
  @prop({ required: true })
  name?: string;

  @prop({ ref: User })
  participants?: Ref<User>[];

  @prop({ required: true })
  publicKey?: string;

  @prop({ ref: Message })
  messages?: Ref<Message>[];
}

export const RoomModel = new Room().getModelForClass(Room, { schemaOptions: { timestamps: true } });

const config = require('./config.json');

export class Config {
  public port: number = config.port || 8080;
  public sslCertFile: string = config.sslCertFile || './certs/public.crt';
  public sslKeyFile: string = config.sslKeyFile || './certs/private.key';
}
